<?php

namespace App\Lib\Denomination;


/**
 * Class UnitStack
 *
 *  - Represents a quantity of Denomination Units (same unit)
 *
 * @package App\Lib\Denomination
 */
class UnitStack
{
    /**
     * @var DenominationInterface
     */
    private $denomination;
    
    /**
     * @var int
     */
    private $quantity;

    /**
     * UnitStack constructor.
     * @param DenominationInterface $denomination
     * @param int $quantity
     * @throws \Exception
     */
    public function __construct(DenominationInterface $denomination, int $quantity = 0)
    {
        if($quantity <= 0) {
            throw new \Exception("Not a valid Quantity");
        }
        $this->denomination = $denomination;
        $this->quantity = $quantity;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'value' => $this->denomination->getValue(),
            'count' => $this->quantity
        ];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return json_encode($this->toArray()) ?? '{}';
    }
}