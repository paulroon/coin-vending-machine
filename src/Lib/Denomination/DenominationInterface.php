<?php

namespace App\Lib\Denomination;

/**
 * Interface DenominationInterface
 * @package App\Lib\Denomination
 */
interface DenominationInterface
{
    /**
     * Returns an array of (smaller) Denomination Values in what Quantity
     * (Totalling current Value)
     * @return array
     */
    public function split(): array;

    /**
     * The Value for the current denomination
     * @return float
     */
    public function getValue(): float;
}