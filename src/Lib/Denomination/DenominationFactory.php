<?php

namespace App\Lib\Denomination;

use Exception;

class DenominationFactory
{

    /**
     * Registered Unit Classes
     * @var array
     */
    private $units = [];

    /**
     *
     * @param array $unitClassList
     */
    public function registerDenominations(array $unitClassList): void
    {
        $this->units = $unitClassList;
    }

    /**
     * @return array
     */
    public function getDenominations(): array
    {
        return $this->units;
    }

    /**
     * @param float $value
     * @return Denomination
     * @throws Exception
     */
    public function createForValue(float $value): Denomination
    {
        /** @var Denomination $denominationUnit */
        foreach($this->units as $denominationUnit) {
            if($denominationUnit::value() === $value) {
                return new $denominationUnit();
            }
        }
        throw new Exception(sprintf("[%s] is not a valid denomination", $value));
    }

    /**
     * @param $unitClass
     * @return mixed
     * @throws Exception
     */
    public function create($unitClass): Denomination
    {
        if(!in_array($unitClass, $this->units)) {
            throw new Exception(sprintf("[%s] is not a valid Unit", $unitClass));
        }
        return new $unitClass();
    }
}