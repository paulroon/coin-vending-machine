<?php

namespace App\Lib\Denomination\Unit;

use App\Lib\Denomination\Denomination;


final class Note50 extends Denomination
{

    /**
     * @inheritdoc
     */
    public static function value(): float
    {
        return 50.0;
    }

    /**
     * @inheritdoc
     */
    public function split(): array
    {
        return [
            Note20::class   => 1,
            Note10::class   => 1,
            Note5::class    => 3,
            Coin100::class  => 3,
            Coin50::class   => 2,
            Coin20::class   => 3,
            Coin10::class   => 4
        ];
    }

}