<?php

namespace App\Lib\Denomination\Unit;

use App\Lib\Denomination\Denomination;


final class Note10 extends Denomination
{

    /**
     * @inheritdoc
     */
    public static function value(): float
    {
        return 10.0;
    }

    /**
     * @inheritdoc
     */
    public function split(): array
    {
        return [
            Note5::class    => 1,
            Coin100::class  => 3,
            Coin50::class   => 2,
            Coin20::class   => 3,
            Coin10::class   => 4
        ];
    }
}