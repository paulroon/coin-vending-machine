<?php

namespace App\Lib\Denomination\Unit;

use App\Lib\Denomination\Denomination;
use Exception;

final class Coin10 extends Denomination
{

    protected $type = self::TYPE_COIN;

    /**
     * @inheritdoc
     */
    public static function value(): float
    {
        return 0.10;
    }

    /**
     * @inheritdoc
     * @throws Exception
     */
    public function split(): array
    {
        throw new Exception(sprintf("%s cannot be split as [%s] is the smallest denomination available", static::class, $this->value()));
    }
}