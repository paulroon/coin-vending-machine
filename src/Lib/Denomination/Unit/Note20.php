<?php

namespace App\Lib\Denomination\Unit;

use App\Lib\Denomination\Denomination;


final class Note20 extends Denomination
{

    /**
     * @inheritdoc
     */
    public static function value(): float
    {
        return 20.0;
    }

    /**
     * @inheritdoc
     */
    public function split(): array
    {
        return [
            Note10::class   => 1,
            Note5::class    => 2
        ];
    }
}