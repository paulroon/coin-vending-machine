<?php

namespace App\Lib\Denomination\Unit;

use App\Lib\Denomination\Denomination;


final class Coin50 extends Denomination
{

    protected $type = self::TYPE_COIN;

    /**
     * @inheritdoc
     */
    public static function value(): float
    {
        return 0.50;
    }

    /**
     * @inheritdoc
     */
    public function split(): array
    {
        return [
            Coin20::class  => 2,
            Coin10::class  => 1
        ];
    }
}