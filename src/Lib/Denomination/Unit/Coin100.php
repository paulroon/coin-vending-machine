<?php

namespace App\Lib\Denomination\Unit;

use App\Lib\Denomination\Denomination;


final class Coin100 extends Denomination
{

    protected $type = self::TYPE_COIN;

    /**
     * @inheritdoc
     */
    public static function value(): float
    {
        return 1.0;
    }

    /**
     * @inheritdoc
     */
    public function split(): array
    {
        return [
            Coin50::class  => 1,
            Coin20::class  => 2,
            Coin10::class  => 1
        ];
    }
}