<?php

namespace App\Lib\Denomination\Unit;

use App\Lib\Denomination\Denomination;

final class Coin20 extends Denomination
{

    protected $type = self::TYPE_COIN;

    /**
     * @inheritdoc
     */
    public static function value(): float
    {
        return 0.20;
    }

    /**
     * @inheritdoc
     */
    public function split(): array
    {
        return [
            Coin10::class  => 2
        ];
    }
}