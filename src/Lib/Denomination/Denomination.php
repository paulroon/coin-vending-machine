<?php

namespace App\Lib\Denomination;

abstract class Denomination implements DenominationInterface
{

    /**
     * UNUSED EXAMPLE
     * I am imagining different input channels / mechanisms
     */
    const TYPE_NOTE = 1;
    const TYPE_COIN = 2;
    const TYPE_CARD = 3;

    protected $type = self::TYPE_NOTE;

    /**
     * Just an example property to show how this might be extended
     * @var string
     */
    protected $currency = "GBP";

    /**
     * @return float
     */
    abstract static function value(): float;

    /**
     * @inheritdoc
     */
    public function getValue(): float
    {
        return static::value();
    }
}