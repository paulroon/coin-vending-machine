<?php

namespace App\Lib\Service;


use App\Lib\Denomination\DenominationFactory;
use App\Lib\Denomination\UnitStack;


class DenominationService
{
    /**
     * @var DenominationFactory
     */
    private $denominationFactory;

    /**
     * DenominationService constructor.
     * @param DenominationFactory $denominationFactory
     */
    public function __construct(DenominationFactory $denominationFactory)
    {
        $this->denominationFactory = $denominationFactory;
    }

    /**
     * @param float $val
     * @return array
     * @throws \Exception
     */
    public function split(float $val): array
    {

        $breakdown = [];
        try {

            $src = $this->denominationFactory->createForValue($val);

            foreach($src->split() as $unitClass => $quantity) {

                $unit = $this->denominationFactory->create($unitClass);
                $unitStack = new UnitStack($unit, $quantity);
                $breakdown[] = $unitStack->toArray();

            }

        } catch (\Exception $e) {
            throw new \Exception(sprintf("Unable to split Value %s", $val));
        }

        return $breakdown;

    }
}