<?php

namespace App\Controller;

use App\Lib\Service\DenominationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/coinvendor/split/{denomination}", name="api_coinvendor_split")
     * @param $denomination
     * @param DenominationService $denominationService
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function index($denomination, DenominationService $denominationService)
    {
        try {

            $bucket = $denominationService->split((float)$denomination);

            return $this->standardApiResponse(['bucket' => $bucket]);

        } catch (\Exception $e) {
            /**
             * NOTE: putting Exception messages into responses is NOT a good idea.
             * In reality i would probably log the detail and put something generic and friendly in the error
             */
            return $this->error($e->getMessage());
        }
    }


    /**
     *
     * V.Minimal implementation
     *
     * @param array $data
     * @param string $message
     * @param int $status
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    private function standardApiResponse(array $data, $message = "ok", $status = Response::HTTP_OK)
    {
        return $this->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ], $status);
    }

    private function error($message, $data = [])
    {
        return $this->standardApiResponse($data, $message, Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
