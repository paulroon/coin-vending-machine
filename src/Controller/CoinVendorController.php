<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CoinVendorController
 * @package App\Controller
 */
class CoinVendorController extends AbstractController
{
    /**
     * @Route("/", name="coin_vendor_app")
     */
    public function index()
    {
        return $this->render('coin-vendor/index.html.twig');
    }
}
