"use strict";

import React from "react";

import '../css/ChangeMachine.scss';

class ChangeMachine extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            wallet: [
                { value: 50 },
                { value: 20 },
                { value: 10 },
                { value: 1 }
            ],
            slot: null,
            bucket: []
        };

        this.denomList = [50, 20, 10, 5, 1, 0.5, 0.1];
    }

    /**
     * Handle Denomination being input for change event
     * @param denomToSplit
     */
    addToSlotHandler(denomToSplit) {

        // Other Handling logic here
        // Logging / Notifications etc...

        this.splitDenomination(denomToSplit);
    };

    /**
     *
     * @param denom
     */
    splitDenomination(denom) {
        /**
         * 1. I'm not too proud of the string replacement here
         *    but in place of a full HATEOAS (or such) API I'm cutting a corner for the demo
         * 2. XHR Requests should probably be farmed out / wrapped in a service object that
         *    deals with more specific use cases / errors etc..
         */
        const url = this.props.config.api_url.replace('--denom--', denom.value);
        fetch(url)
            .then((response) => {
                return response.json();
            })
            .then((myJson) => {
                this.setState({ bucket: myJson.data.bucket, slot: denom});
            });
    }



    render() {

        /**
         * The Following JSX snippets would likely end up becoming their own
         * React Components - but there's no real need at this point. As the spec is too simple.
         */

        const wallet = [...this.state.wallet].map((denom) => (
            <div key={denom.value} className='denom' onClick={this.addToSlotHandler.bind(this, denom)} >{ denom.value }</div>
        ));

        const bucket = [...this.state.bucket].map((denomStack, dIndex) => (
            <div key={dIndex} className='denomStack'>
                <div className='denomCount'><a href='#'>{denomStack.count}</a></div>
                <div className='denomValue'>{denomStack.value}</div>
            </div>
        ));

        const slot = (
            <div className='denom' >{ this.state.slot ? this.state.slot.value : null }</div>
        );

        return (
            <div id='ChangeMachine'>
                <div id='header'>Click Denominations to add them to the Machine Slot and dispense Change.</div>
                <div id='wallet'>
                    { wallet }
                </div>
                <div id='slot'>Slot { slot }</div>
                <div id="bucket">
                    { bucket }
                </div>
            </div>
        );
    }
}

export default ChangeMachine;