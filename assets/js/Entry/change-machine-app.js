'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import ChangeMachine from '../../Apps/ChangeMachine/components/ChangeMachine';

document.addEventListener('DOMContentLoaded', () => {

  const rootEl = document.getElementById('changemachine-app');
  const config = {
    api_url: rootEl.getAttribute('data-api-url')
  };

  ReactDOM.render(<ChangeMachine config={config}/>, rootEl);

});
