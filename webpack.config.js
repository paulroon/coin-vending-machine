const Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')

    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())

    .createSharedEntry('global', ['./assets/js/Entry/global-app.js'])
    .addEntry('change-machine-app', ['./assets/js/Entry/change-machine-app.js'])

    .enableSassLoader()
    .enableReactPreset()

    .autoProvidejQuery()

;

module.exports = Encore.getWebpackConfig();
