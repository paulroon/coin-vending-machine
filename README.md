Coin Vending Machine
====================

## Overview

The Coin Vending Machine, is a a minimal working application that demonstrates best practices when developing 
a PHP / Symfony 4 Web Application with a React Front end app.

#### Specification / Requirements

```text
Coin Vending Machine
We need a coin vending machine that can accept different denominations 
of sterling and exchange them for smaller denominations  (e.g. a change machine). 
It accepts notes (£50, £20, £10, £5) and coins (£1, 50p 20p 10p) pieces.
 
It needs to be able to produce change as follows:
 
If the user enters a £50 note – it should produce a 1 x £20, 1x £10, 2 x £5 note, 3 x £1 coins, 2 x 50p, 3x20p, 4x10p
If the user enters a £20 note – it should produce a 1x £10 note,2 x £5 note
If the user enters a £10 note – it should produce a £5 note, 3 x £1 coins, 2 x 50p, 3x20p, 4x10p
If the user enters £1 coin – it should produce 1 x 50p, 2x20p, 1x10p
```


### Observations & Development Notes

Hopefully the code explains itself, but here's a few things I wrote down while building.

##### Back-End

 - The Spec was revised to correct the obvious error in the breakdown of a £50 note. I assumed 3 x £5 notes instead of 2, this would have been raised and discussed in a normal team dev process.
 - I am using the terminology "Denomination" to define a note or coin of a specific value.
 - I use the terminology "Unit" to define a specific instance of a currency denomination. i.e 2 x £5 notes are 2 units of denomination 5.
 - I have (as far as possible) de-coupled the application code/logic from Symfony - Portability might be important later.
 - I chose to explicitly define each Note / Coin in its own class as they are (in the real world) representations very likely to either not change at all or remain a maintainable set.
 Implicitly defining them based on constructor configuration would have led to less flexibility when maintaining individual cases.
 - Configuration: There are areas where application config might make sense here, however I prefer not to over-engineer upfront, that way the problem can be better understood as it scales.
 - There is an API here - it has a single endpoint and is not strictly RESTful. I did not elaborate on this aspect as what is here could be considered a disposable placeholder pending formal API design.
 - PHPUnit is used for functional & Unit tests. I would also lean towards a more behavioral framework for a larger project - perhaps behat?
  
##### Front-End
 
 - Single Page Twig template rendering a React App. 
 - React was not a core requirement so this is more of a teaser than code demo.
 - No Front-end tests supplied - honestly, I haven't tested React before.
 
####  Dependencies

You will need docker & docker-compose installed

#### Install

```bash
 $> cd ~/[DEV_FOLDER]

 $> git clone git@bitbucket.org:paulroon/coin-vending-machine.git

 $> cd ./coin-vending-machine
```

#### Start the containers

```bash
 $> docker-compose up -d 
```

#### To work in the PHP-fpm container

```bash
 $> docker-compose exec php-fpm bash

 $> cd /application
```

*example*::
```bash
 $> composer install
```

#### Front-End
The front-end assets are packaged using webpack (via symfony Encore)

I use **yarn** to manage node_modules, **npm** will work too.

To make your initial build for the app.
```bash
 $> yarn install
 $> yarn build
```


#### Testing

Just run ::
```bash
 $> ./bin/phpunit
```

#### Other info

 - Development site is available on http://localhost:8000/
 - scss is configured
 
 When working on ***JS** + **S/CSS*** use:
 
```bash
 $> yarn run encore dev --watch
```
to watch for changes.. 
