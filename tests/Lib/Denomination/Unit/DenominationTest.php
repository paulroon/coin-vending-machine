<?php

namespace App\Lib\Denomination\Unit;


use App\Lib\Denomination\Denomination;
use App\Lib\Denomination\DenominationInterface;
use PHPUnit\Framework\TestCase;

class DenominationTest extends TestCase
{

    public $splittableDenominations = [
        Note50::class,
        Note20::class,
        Note10::class,
        Note5::class,
        Coin100::class,
        Coin50::class,
        Coin20::class
    ];

    public $smallest = Coin10::class;

    /**
     * Helper
     * @param bool $incSmallest
     * @return array
     */
    public function getDenominations($incSmallest = false)
    {
        return $incSmallest
            ? array_merge($this->splittableDenominations, [$this->smallest])
            : $this->splittableDenominations;
    }

    /**
     * @param $className
     * @return Denomination
     */
    public function getDenominationInstance($className): Denomination
    {
        return new $className();
    }

    /**
     * Make sure all Named Denominations Inherit correctly
     */
    public function testAllDenominationsBuildOK()
    {
        foreach($this->getDenominations(true) as $denomClassName) {
            $unit = $this->getDenominationInstance($denomClassName);

            $this->assertInstanceOf(DenominationInterface::class, $unit);
            $this->assertInstanceOf(Denomination::class, $unit);

        }
    }

    /**
     * Check all Denominations are yielding a split result
     */
    public function testSplitYieldsAResult()
    {
        foreach($this->getDenominations(false) as $denomClassName) {

            /** @var Denomination $unit */
            $unit = $this->getDenominationInstance($denomClassName);

            $change = $unit->split();
            if($denomClassName !== $this->smallest) {
                $this->assertEquals(is_array($change), true);
                $this->assertNotEmpty($change);
            }

        }
    }

    public function testSmallestSplitThrowsException()
    {
        /** @var Denomination $unit */
        $unit = $this->getDenominationInstance($this->smallest);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage(
            sprintf(
                "%s cannot be split as [%s] is the smallest denomination available",
                $this->smallest,
                $unit->getValue()
            )
        );

        $unit->split();
    }

    /**
     * Check split denominations total the original Value
     */
    public function testSplitTotalsCorrectly()
    {
        foreach($this->getDenominations(false) as $denomClassName) {
            $srcValue = $denomClassName::value();
            /** @var Denomination $unit */
            $unit = $this->getDenominationInstance($denomClassName);
            $change = $unit->split();
            $subTotal = 0;
            foreach($change as $subDenom => $quantity) {
                $this->assertGreaterThan(0, $quantity);
                $subTotal += $subDenom::value() * $quantity;
            }
            $this->assertEquals($srcValue, $subTotal);
        }
    }

    /**
     * Check getValues instance method hasn't modified the static config
     */
    public function testGetValueAgrees()
    {
        foreach($this->getDenominations(false) as $denomClassName) {
            $unit = $this->getDenominationInstance($denomClassName);
            $this->assertEquals($denomClassName::value(), $unit->getValue());
        }
    }

}
