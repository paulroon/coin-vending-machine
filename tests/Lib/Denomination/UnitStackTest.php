<?php

namespace App\Lib\Denomination;


use App\Lib\Denomination\Unit\Note50;
use PHPUnit\Framework\TestCase;

class UnitStackTest extends TestCase
{

    /** @var UnitStack $unitStack */
    private $unitStack;

    protected function setUp()
    {
        $n50 = new Note50();
        $this->unitStack = new UnitStack($n50, 10);
    }

    /**
     *
     */
    public function testFailConstructDenomination()
    {
        $valid = 1;
        $this->expectException(\TypeError::class);
        $us = new UnitStack(new \StdClass(), 10);
    }

    /**
     *
     */
    public function testFailConstructQuantity()
    {
        $invalidQuantityMessage = "Not a valid Quantity";
        foreach ([-1, 0] as $badQuantity) {
            $this->expectExceptionMessage($invalidQuantityMessage);
            new UnitStack(new Note50(), $badQuantity);
        }
        $this->expectExceptionMessage($invalidQuantityMessage);
        new UnitStack(new Note50(), "NotEven_an_Integer");
    }

    public function testToArray()
    {
        $arr = $this->unitStack->toArray();
        $this->assertTrue(is_array($arr));
        $this->assertArrayHasKey("value", $arr);
        $this->assertArrayHasKey("count", $arr);
        $this->assertEquals(Note50::value(), $arr['value']);
        $this->assertEquals(10, $arr['count']);
    }

    public function test__toString()
    {
        $jsonStr = (string) $this->unitStack;
        $this->assertEquals(json_encode(['value' => Note50::value(), 'count' => 10]), $jsonStr);
    }
}
