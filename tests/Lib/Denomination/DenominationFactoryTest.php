<?php

namespace App\Lib\Denomination;


use App\Lib\Denomination\Unit\Coin10;
use App\Lib\Denomination\Unit\Note20;
use App\Lib\Denomination\Unit\Note50;
use PHPUnit\Framework\TestCase;

class DenominationFactoryTest extends TestCase
{

    /** @var DenominationFactory $denominationFactory */
    private $denominationFactory;

    protected function setUp()
    {
        $this->denominationFactory = new DenominationFactory();
        $this->denominationFactory->registerDenominations([
            Note50::class,
            Note20::class,
            Coin10::class
        ]);

    }

    public function testRegisterDenominations()
    {
        $factory = new DenominationFactory();
        $this->assertInstanceOf(DenominationFactory::class, $factory);

        $registeredDenominations = $factory->getDenominations();

        $this->assertEmpty($registeredDenominations);

        $factory->registerDenominations([Note50::class, Coin10::class]);
        $newRegisteredDenominations = $factory->getDenominations();

        $this->assertTrue(is_array($newRegisteredDenominations));
        $this->assertNotEmpty($newRegisteredDenominations);
        $this->assertCount(2, $newRegisteredDenominations);
        $this->assertEquals($newRegisteredDenominations[0], Note50::class);
        $this->assertEquals($newRegisteredDenominations[1], Coin10::class);
    }

    public function testCreate()
    {
        /** @var Denomination $n50 */
        $n50 = $this->denominationFactory->create(Note50::class);
        $this->assertInstanceOf(Note50::class, $n50);
        $this->assertEquals(50.0, $n50->getValue());

    }

    public function testCreateForValue()
    {
        $value = 50.0;
        /** @var Denomination $n50 */
        $n50 = $this->denominationFactory->createForValue($value);
        $this->assertInstanceOf(Note50::class, $n50);
        $this->assertEquals($value, $n50->getValue());
    }


    public function testFailNonDenomination()
    {
        $otherClass = "\stdClass";
        $this->expectExceptionMessage(sprintf("[%s] is not a valid Unit", $otherClass));
        $this->denominationFactory->create($otherClass);
    }

    /**
     * integers should be implicitly cast to the required float
     * so expect this to pass
     * @throws \Exception
     */
    public function testIntegerValueCastCreation()
    {
        $integerValue = (int) 50;

        /** @var Denomination $n50 */
        $n50 = $this->denominationFactory->createForValue($integerValue);

        $this->assertInstanceOf(Note50::class, $n50);
        $this->assertEquals($integerValue, $n50->getValue());
    }


    /**
     * strings should be implicitly cast to the required float
     * so expect this to pass
     * @throws \Exception
     */
    public function testStringValueCastCreation()
    {
        foreach (["50", "50.0"] as $stringValue) {

            $stringValue = (string) $stringValue;

            /** @var Denomination $n50 */
            $n50 = $this->denominationFactory->createForValue($stringValue);

            $this->assertInstanceOf(Note50::class, $n50);
            $this->assertEquals($stringValue, $n50->getValue());
        }

        foreach (["oops", "Fifty", "fifty.zero"] as $failStringValue) {
            $failStringValue = (string) $failStringValue;

            $this->expectException(\TypeError::class);
            /** @var Denomination $failUnit */
            $this->denominationFactory->createForValue($failStringValue);

        }

    }


    /**
     * booleans should be implicitly cast to the required float
     * so expect this to pass
     * @throws \Exception
     */
    public function testFailsBoolValueCreation()
    {
        $boolValue = (bool) true;

        $this->expectExceptionMessage(sprintf("[%s] is not a valid denomination", $boolValue ? '1' : '0'));
        $this->denominationFactory->createForValue($boolValue);
    }

}
