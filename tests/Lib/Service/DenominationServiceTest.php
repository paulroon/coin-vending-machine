<?php

namespace App\Lib\Service;


use App\Lib\Denomination\DenominationFactory;
use App\Lib\Denomination\Unit\Coin10;
use App\Lib\Denomination\Unit\Coin100;
use App\Lib\Denomination\Unit\Coin20;
use App\Lib\Denomination\Unit\Coin50;
use App\Lib\Denomination\Unit\Note10;
use App\Lib\Denomination\Unit\Note20;
use App\Lib\Denomination\Unit\Note5;
use App\Lib\Denomination\Unit\Note50;
use PHPUnit\Framework\TestCase;


class DenominationServiceTest extends TestCase
{

    /** @var DenominationService $denominationService */
    private $denominationService;
    private $correctValue;

    protected function setUp()
    {

        $factory = new DenominationFactory();
        $factory->registerDenominations([
            Note50::class, Note20::class, Note10::class, Note5::class,
            Coin100::class, Coin50::class, Coin20::class, Coin10::class
        ]);

        $this->denominationService = new DenominationService($factory);
        $this->correctValue = 50.0;
    }


    /**
     * From Original Spec::
     *
     * 50 //it should produce a 1 x £20, 1x £10, 2 x £5 note, 3 x £1 coins, 2 x 50p, 3x20p, 4x10p
     *      - [corrected to 3 x £5]
     * 20 // it should produce a 1x £10 note,2 x £5 note
     * 10 //it should produce a £5 note, 3 x £1 coins, 2 x 50p, 3x20p, 4x10p
     * 1  //it should produce 1 x 50p, 2x20p, 1x10p
     * @test
     */
    public function split_denomination_quantities_as_per_spec()
    {
        $expectation = [
            '50' => ['20'=>1,'10'=>1,'5'=>3,'1'=>3,'0.5'=>2,'0.2'=>3,'0.1'=>4],
            '20' => ['10'=>1,'5'=>2],
            '10' => ['5'=>1,'1'=>3,'0.5'=>2,'0.2'=>3,'0.1'=>4],
            '1' =>  ['0.5'=>1,'0.2'=>2,'0.1'=>1]
        ];
        foreach($expectation as $srcQuantity => $breakdown) {
            
            $actual = $this->denominationService->split($srcQuantity);

            $this->assertTrue(is_array($actual));
            $this->assertCount(count($breakdown), $actual);
            $this->assertEquals(array_keys($breakdown), array_column($actual, 'value'));
            $this->assertEquals(array_values($breakdown), array_column($actual, 'count'));

        }
    }

    /** @test */
    public function fails_gracefully_with_bad_input()
    {

        $incorrectInputs = [null, "Batman"];
        foreach ($incorrectInputs as $incorrectInput) {
            $this->expectException(\TypeError::class);
            $r = $this->denominationService->split($incorrectInput);
        }

    }

    /** @test */
    public function fails_gracefully_with_non_configured_input()
    {
        $badInputs = [-1, 17];
        foreach ($badInputs as $badInput) {
            $this->expectExceptionMessage(sprintf("Unable to split Value %", $badInput));
            $this->denominationService->split($badInput);
        }

    }
}
