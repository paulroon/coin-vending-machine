<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Client;
use Symfony\Component\HttpFoundation\Response;

class ApiControllerTest extends WebTestCase
{

    /** @var Client */
    private $client;
    private $router;

    protected function setUp()
    {
        $this->client = static::createClient();
        $this->router = $this->client->getContainer()->get('router');
    }

    public function getEndpoint($routeName, $args = [])
    {
        $this->client->request('GET', $this->router->generate($routeName, $args, false));
        return $this->client->getResponse();
    }


    /** @test */
    public function api_is_responding()
    {

        $response = $this->getEndpoint('api_coinvendor_split', ['denomination' => 50]);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());


    }


    /** @test */
    public function api_response_has_standardised_format()
    {
        $response = $this->getEndpoint('api_coinvendor_split', ['denomination' => 50]);

        $responseContent = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('status', $responseContent);
        $this->assertArrayHasKey('message', $responseContent);
        $this->assertArrayHasKey('data', $responseContent);
        $this->assertArrayHasKey('bucket', $responseContent['data']);
    }

    /** @test */
    public function split_endpoint_has_bucket_with_n_values_and_counts()
    {
        $response = $this->getEndpoint('api_coinvendor_split', ['denomination' => 50]);

        $bucket = json_decode($response->getContent(), true)['data']['bucket'];

        $this->assertCount(7, $bucket);
        foreach($bucket as $unitStack) {
            $this->assertArrayHasKey('value', $unitStack);
            $this->assertArrayHasKey('count', $unitStack);
        }
    }


}